import 'dart:async';

import 'package:flutter/services.dart';

class BugTag {
  static const MethodChannel _channel = const MethodChannel('bugtag');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<void> initBugTag(
      {String androidKey = "",
      String iosKey = "",
      bool enableLog = true}) async {
    Map<String, dynamic> args = <String, dynamic>{};
    args.putIfAbsent("api_key_android", () => androidKey);
    args.putIfAbsent("api_key_ios", () => iosKey);
    args.putIfAbsent("is_log_enabled", () => enableLog);

    await _channel.invokeMethod('initBugTag', args);
  }

  static Future<void> setUser(String userId) async {
    Map<String, dynamic> args = <String, dynamic>{};
    args.putIfAbsent("userId", () => userId);
    await _channel.invokeMethod('setUserId', args);
  }
}
