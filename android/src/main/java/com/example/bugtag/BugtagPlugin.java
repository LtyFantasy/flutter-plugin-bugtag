package com.example.bugtag;

import android.app.Application;
import android.content.Context;
import android.util.Log;


import com.bugtags.library.Bugtags;
import com.bugtags.library.BugtagsOptions;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/**
 * FlurryPlugin
 */
public class BugtagPlugin implements MethodCallHandler {

    private Application application;
    private MethodChannel channel;

    /**
     * Plugin registration.
     */
    public static void registerWith(Registrar registrar) {
        final MethodChannel channel = new MethodChannel(registrar.messenger(), "bugtag");
        channel.setMethodCallHandler(new BugtagPlugin(registrar.context(), channel));
    }


    public BugtagPlugin(Context application, MethodChannel channel) {
        this.application = (Application) application;
        this.channel = channel;
        this.channel.setMethodCallHandler(this);
    }


    @Override
    public void onMethodCall(MethodCall call, final Result result) {
        if (call.method.equals("getPlatformVersion")) {
            result.success("Android ${android.os.Build.VERSION.RELEASE}");
        } else if (call.method.equals("initBugTag")) {
            String apiKey = call.argument("api_key_android");
            if (BuildConfig.DEBUG) {
                Bugtags.start(apiKey, application, Bugtags.BTGInvocationEventBubble);
            } else {
                Bugtags.start(apiKey, application, Bugtags.BTGInvocationEventNone);
            }
            result.success(null);
        } else if (call.method.equals("setUserId")) {
            String userId = call.argument("userId");
            Bugtags.setUserData("userId", userId);
            result.success(null);
        } else {
            result.notImplemented();
        }
    }
}

